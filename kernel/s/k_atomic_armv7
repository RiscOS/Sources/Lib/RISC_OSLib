; Copyright 2022 RISC OS Open Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

; Assumptions:
; * ARMv7+
; * All atomic types are aligned to their size

        MACRO
        BARRIER_ARMv7 $reg,$flags
        TST     $reg, #$flags
        BEQ     %FT50
      [ $flags == BARRIER_DMB_STR
        DMB     ST
      |
        ; TODO: Optimise the other cases to only do ST where possible
        DMB
      ]
50
        MEND

; It's easiest to generate most of these functions using a giant WHILE loop
size    SETA    1
        WHILE   size <= 8

; Set $sz to the correct size suffix
      [ size = 1
sz      SETS    "B"
      ELIF size = 2
sz      SETS    "H"
      ELIF size = 4
sz      SETS    ""
      |
sz      SETS    "D"
      ]

sz2     SETS    (:STR:size) :RIGHT: 1

; void _kernel_atomic_store_N(volatile void* obj, memory_order order, C desired);
      [ :DEF: Need__kernel_atomic_store_$sz2._ARMv7
        FUNC    _kernel_atomic_store,,_ARMv7
        BARRIER_ARMv7 a2, BARRIER_DMB_STR
      [ size < 8
        STR$sz  a3, [a1]
        MOV     pc, lr
      |
        FunctionEntry "v1-v2"
10
        LDREXD  v1, v2, [a1] ; Dummy load to prime the exclusive monitor
        STREXD  a2, a3, a4, [a1]
        TEQ     a2, #STREX_succeeded
        BNE     %BT10
        Return  "v1-v2"
      ]
      ]

; C _kernel_atomic_load_N(volatile void* obj, memory_order order);
      [ :DEF: Need__kernel_atomic_load_$sz2._ARMv7
        FUNC    _kernel_atomic_load,,_ARMv7
      [ size < 8
        LDR$sz  a1, [a1]
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB
      |
10
        ; ARM ARM approved way of performing an atomic 64bit load is to use LDREX/STREX to write back the read value
        LDREXD  a3, a4, [a1]
        STREXD  ip, a3, a4, [a1]
        TEQ     ip, #STREX_succeeded
        BNE     %BT10
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB
        MOV     a1, a3
        MOV     a2, a4
      ]
        MOV     pc, lr
      ]

; C _kernel_atomic_exchange_N(volatile void* obj, memory_order order, C desired);
      [ :DEF: Need__kernel_atomic_exchange_$sz2._ARMv7
        FUNC    _kernel_atomic_exchange,,_ARMv7
      [ size < 8
50
        LDREX$sz ip, [a1]
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        STREX$sz a4, a3, [a1]
        TEQ     a4, #STREX_succeeded
        BNE     %BT50
        MOV     a1, ip
        MOV     pc, lr
      |
        FunctionEntry "v1-v2"
50
        LDREXD  v1, v2, [a1]
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        STREXD  ip, a3, a4, [a1]
        TEQ     ip, #STREX_succeeded
        BNE     %BT50
        MOV     a1, v1
        MOV     a2, v2
        Return  "v1-v2"
      ]
      ]

; _Bool _kernel_atomic_compare_exchange_weak_N(volatile void* obj, int orders, C* expected, C desired);
; 'orders' is the success memory order + failure memory order<<4
      [ :DEF: Need__kernel_atomic_compare_exchange_weak_$sz2._ARMv7
        FUNC    _kernel_atomic_compare_exchange_weak,,_ARMv7
      [ size < 8
        FunctionEntry
        LDR$sz   lr, [a3]
        LDREX$sz ip, [a1]
        TEQ     ip, lr
        BNE     %F05
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        STREX$sz ip, a4, [a1]
        TEQ     ip, #STREX_succeeded
        MOVEQ   a1, #1
        MOVNE   a1, #0
        Return
05
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB:SHL:4
        STR$sz  ip, [a3]
        MOV     a1, #0
        Return
      |
        FunctionEntry "v1-v6",makeframe
        MOV     v5, a4
        LDR     v6, [fp, #4] ; Upper half of 'desired'
        LDMIA   a3, {v1, v2}
        LDREXD  v3, v4, [a1]
        TEQ     v1, v3
        TEQEQ   v2, v4
        BNE     %FT85
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        STREXD  ip, v5, v6, [a1]
        TEQ     ip, #STREX_succeeded
        MOVEQ   a1, #1
        MOVNE   a1, #0
        Return  "v1-v6",fpbased
85
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB:SHL:4
        STMIA   a3, {v3, v4}
        MOV     a1, #0
        Return  "v1-v6",fpbased
      ]
      ]

; _Bool _kernel_atomic_compare_exchange_strong_N(volatile void* obj, int orders, C* expected, C desired);
; Type is the success memory order + failure memory order<<4
      [ :DEF: Need__kernel_atomic_compare_exchange_strong_$sz2._ARMv7
        FUNC    _kernel_atomic_compare_exchange_strong,,_ARMv7
      [ size < 8
        FunctionEntry
        LDR$sz   lr, [a3]
01
        LDREX$sz ip, [a1]
        TEQ     ip, lr
        BNE     %F05
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        STREX$sz ip, a4, [a1]
        TEQ     ip, #STREX_succeeded
        BNE     %B01
        MOV     a1, #1
        Return
05
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB:SHL:4
        STR$sz  ip, [a3]
        MOV     a1, #0
        Return
      |
        FunctionEntry "v1-v6",makeframe
        MOV     v5, a4
        LDR     v6, [fp, #4] ; Upper half of 'desired'
        LDMIA   a3, {v1, v2}
81
        LDREXD  v3, v4, [a1]
        TEQ     v1, v3
        TEQEQ   v2, v4
        BNE     %FT85
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        STREXD  ip, v5, v6, [a1]
        TEQ     ip, #STREX_succeeded
        BNE     %BT81
        MOV     a1, #1
        Return  "v1-v6",fpbased
85
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB:SHL:4
        STMIA   a3, {v3, v4}
        MOV     a1, #0
        Return  "v1-v6",fpbased
      ]
      ]

; C _kernel_atomic_fetch_add_N(volatile void* obj, memory_order order, M arg);
      [ :DEF: Need__kernel_atomic_fetch_add_$sz2._ARMv7
        FUNC    _kernel_atomic_fetch_add,,_ARMv7
      [ size < 8
01      LDREX$sz ip, [a1]
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        ADD     ip, ip, a3
        STREX$sz a4, ip, [a1]
        TEQ     a4, #STREX_succeeded
        BNE     %B01
        SUB     a1, ip, a3
        MOV     pc, lr
      |
        FunctionEntry "v1-v2"
80
        LDREXD  v1, v2, [a1]
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        ADDS    v1, v1, a3
        ADC     v2, v2, a4
        STREXD  ip, v1, v2, [a1]
        TEQ     ip, #STREX_succeeded
        BNE     %BT80
        SUBS    a1, v1, a3
        SBC     a2, v2, a4
        Return  "v1-v2"
      ]
      ]

; fetch_xor, fetch_or, fetch_and are implemented using a routine which performs
; an AND & XOR pair. This helps cut down the number of routines needed, without
; slowing things down too much.

; C _kernel_atomic_fetch_xor_N(volatile void* obj, memory_order order, M arg);
; Type is the size + memory order<<4
      [ :DEF: Need__kernel_atomic_fetch_xor_$sz2._ARMv7
        FUNC    _kernel_atomic_fetch_xor,,_ARMv7
      [ size < 8
        FunctionEntry "v1"
        MVN     v1, #0
        B       andxor_ARMv7_$sz2
      |
        FunctionEntry "v1-v5"
        MVN     v4, #0
        MVN     v5, #0
        B       andxor_ARMv7_$sz2
      ]
      ]

; C _kernel_atomic_fetch_or_N(volatile void* obj, memory_order order, M arg);
      [ :DEF: Need__kernel_atomic_fetch_or_$sz2._ARMv7
        FUNC    _kernel_atomic_fetch_or,,_ARMv7
      [ size < 8
        FunctionEntry "v1"
        MVN     v1, a3
        B       andxor_ARMv7_$sz2
      |
        FunctionEntry "v1-v5"
        MVN     v4, a3
        MVN     v5, a4
        B       andxor_ARMv7_$sz2
      ]
      ]

; C _kernel_atomic_fetch_and_N(volatile void* obj, memory_order order, M arg);
      [ :DEF: Need__kernel_atomic_fetch_and_$sz2._ARMv7
        FUNC    _kernel_atomic_fetch_and,,_ARMv7
      [ size < 8
        FunctionEntry "v1"
        MOV     v1, a3
        MOV     a3, #0
        B       andxor_ARMv7_$sz2
      |
        FunctionEntry "v1-v5"
        MOV     v4, a3
        MOV     v5, a4
        MOV     a3, #0
        MOV     a4, #0
        B       andxor_ARMv7_$sz2
      ]
      ]

      [ :DEF: Need__kernel_atomic_fetch_xor_$sz2._ARMv7 :LOR: :DEF: Need__kernel_atomic_fetch_or_$sz2._ARMv7 :LOR: :DEF: Need__kernel_atomic_fetch_and_$sz2._ARMv7
andxor_ARMv7_$sz2
      [ size < 8
01      LDREX$sz ip, [a1]
        BARRIER_ARMv7 a2, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        AND     lr, ip, v1
        EOR     lr, lr, a3
        STREX$sz a4, lr, [a1]
        TEQ     a4, #STREX_succeeded
        BNE     %B01
        MOV     a1, ip
        Return  "v1"
      |
        MOV     ip, a1
        MOV     lr, a2
80
        LDREXD  a1, a2, [ip]
        BARRIER_ARMv7 lr, BARRIER_LDR_DMB :OR: BARRIER_DMB_STR
        AND     v1, a1, v4
        AND     v2, a2, v5
        EOR     v1, v1, a3
        EOR     v2, v2, a4
        STREXD  v3, v1, v2, [ip]
        TEQ     v3, #STREX_succeeded
        BNE     %B80
        Return  "v1-v5"
      ]
      ]

size    SETA size*2
        WEND

; void _kernel_atomic_thread_fence(memory_order order);
      [ :DEF: Need__kernel_atomic_thread_fence_ARMv7
        EXPORT  |_kernel_atomic_thread_fence_ARMv7|
|_kernel_atomic_thread_fence_ARMv7|
        ; Assume nobody's going to be calling this with RELAXED order
        TST     a1, #BARRIER_LDR_DMB
        BNE     %FT50
        DMB     ST
        MOV     pc, lr
50
        DMB
        MOV     pc, lr
      ]

; _Bool _kernel_atomic_flag_test_and_set_explicit(volatile atomic_flag *obj, memory_order order);
      [ :DEF: Need__kernel_atomic_flag_test_and_set_explicit_ARMv7
        EXPORT  |_kernel_atomic_flag_test_and_set_explicit_ARMv7|
|_kernel_atomic_flag_test_and_set_explicit_ARMv7|
        ; exchange_4 is always lock-free, call through to it
        MOV     a3, #1
        B       _kernel_atomic_exchange_4_ARMv7
      ]

; void _kernel_atomic_flag_clear_explicit(volatile atomic_flag *obj, memory_order order);
      [ :DEF: Need__kernel_atomic_flag_clear_explicit_ARMv7
        EXPORT  |_kernel_atomic_flag_clear_explicit_ARMv7|
|_kernel_atomic_flag_clear_explicit_ARMv7|
        ; store_4 is always lock-free, call through to it
        MOV     a3, #0
        B       _kernel_atomic_store_4_ARMv7
      ]

        END
