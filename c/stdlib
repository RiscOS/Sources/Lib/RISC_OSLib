/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* stdlib.c: ANSI draft (X3J11 Oct 86) library, section 4.10 */
/* Copyright (C) Codemist Ltd, 1988 */
/* version 0.02a */

#include "hostsys.h"      /* for _terminateio(), and _exit() etc */
#include "kernel.h"
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <stdint.h>
#include "alloc.h"

/* atof, atoi, atol, strtod, strtol, strtoul are implemented in scanf.c  */

/* mblen, mbtowc, wctomb, mbstowcs, wcstombs are implemented in locale.c */

/* div and ldiv are implemented in machine code */

static unsigned long int next = 1;

int _ANSI_rand(void)     /* This is the ANSI suggested portable code */
{
    next = next * 1103515245 + 12345;
    return (unsigned int) (next >> 16) % 32768;
}

void _ANSI_srand(unsigned int seed)
{
    next = seed;
}

/* Now the random-number generator that the world is expected to use */

static unsigned _random_number_seed[55] =
/* The values here are just those that would be put in this horrid
   array by a call to __srand(1). DO NOT CHANGE __srand() without
   making a corresponding change to these initial values.
*/
{   0x00000001, 0x66d78e85, 0xd5d38c09, 0x0a09d8f5, 0xbf1f87fb,
    0xcb8df767, 0xbdf70769, 0x503d1234, 0x7f4f84c8, 0x61de02a3,
    0xa7408dae, 0x7a24bde8, 0x5115a2ea, 0xbbe62e57, 0xf6d57fff,
    0x632a837a, 0x13861d77, 0xe19f2e7c, 0x695f5705, 0x87936b2e,
    0x50a19a6e, 0x728b0e94, 0xc5cc55ae, 0xb10a8ab1, 0x856f72d7,
    0xd0225c17, 0x51c4fda3, 0x89ed9861, 0xf1db829f, 0xbcfbc59d,
    0x83eec189, 0x6359b159, 0xcc505c30, 0x9cbc5ac9, 0x2fe230f9,
    0x39f65e42, 0x75157bd2, 0x40c158fb, 0x27eb9a3e, 0xc582a2d9,
    0x0569d6c2, 0xed8e30b3, 0x1083ddd2, 0x1f1da441, 0x5660e215,
    0x04f32fc5, 0xe18eef99, 0x4a593208, 0x5b7bed4c, 0x8102fc40,
    0x515341d9, 0xacff3dfa, 0x6d096cb5, 0x2bb3cc1d, 0x253d15ff
};

static int _random_j = 23, _random_k = 54;

int rand(void)
{
/* See Knuth vol 2 section 3.2.2 for a discussion of this random
   number generator.
*/
    unsigned int temp;
    temp = (_random_number_seed[_random_k] += _random_number_seed[_random_j]);
    if (--_random_j == 0) _random_j = 54, --_random_k;
    else if (--_random_k == 0) _random_k = 54;
    return (temp & 0x7fffffff);         /* result is a 31-bit value */
    /* It seems that it would not be possible, under ANSI rules, to */
    /* implement this as a 32-bit value. What a shame!              */
}

void srand(unsigned int seed)
{
/* This only allows you to put 32 bits of seed into the random sequence,
   but it is very improbable that you have any good source of randomness
   that good to start with anyway! A linear congruential generator
   started from the seed is used to expand from 32 to 32*55 bits.
*/
    int i;
    _random_j = 23;
    _random_k = 54;
    for (i = 0; i<55; i++)
    {   _random_number_seed[i] = seed + (seed >> 16);
/* This is not even a good way of setting the initial values.  For instance */
/* a better scheme would have r<n+1> = 7^4*r<n> mod (3^31-1).  Still I will */
/* leave this for now.                                                      */
        seed = 69069*seed + 1725307361;  /* computed modulo 2^32 */
    }
}

/* free, malloc, realloc etc are in the file alloc.c                     */

/* Big enough that static allocation (which can't fail) allows the 32       */
/* required by the C standard, plus _clean_up() which is potentially        */
/* registered twice                                                         */
#define EXIT_LIMIT 34

typedef void (*vprocp)(void);
struct exitvector
{
    union { vprocp p[EXIT_LIMIT]; uintptr_t i[EXIT_LIMIT]; };
    struct exitvector *prev;
    struct exitvector *next;
};
static struct exitvector _exitvector = { 0 };
static struct exitvector _qxitvector = { 0 };
       /* initialised so not in bss (or shared library trouble) */
static struct exitvector *_last_exitvector;
static struct exitvector *_last_qxitvector;

void _exit_init(void)
{
    if (!_kernel_client_is_module())
    {
        /* Module-client initialisation, or application client */
        _last_exitvector = &_exitvector;
        _last_qxitvector = &_qxitvector;
    }
}

static int atexit_common(vprocp func, struct exitvector **last_vector, int at_module_final)
{
    struct exitvector *vector = *last_vector;
    size_t i = 0;
    if (vector->p[EXIT_LIMIT-1])
    {   /* Need to spill to another vector. */
        /* For module clients, it's possible that empty vectors remain left over
         * from earlier invocations of main(). */
        if (vector->next)
        {
            *last_vector = vector = vector->next;
        }
        else
        {
            struct exitvector *next_vector;
            if ((_kernel_processor_mode() & 0xF) != 0 || _kernel_client_is_module())
                /* For module clients, these need to be allocated from the RMA because
                 * we may need to navigate them from SVC mode from any task at any time */
                next_vector = _rma_malloc(sizeof *next_vector);
            else
                next_vector = malloc(sizeof *next_vector);
            if (next_vector == NULL)
                return 1; /* failure */
            memset(next_vector, 0, sizeof *next_vector);
            next_vector->prev = vector;
            *last_vector = vector = vector->next = next_vector;
        }
    }
    else
    {
        /* Rather than using even more static data to store the latest offset
         * into the vector, simply search backwards for the last non-NULL pointer */
        for (i = EXIT_LIMIT-1; i > 0; --i)
            if (vector->p[i-1])
                break;
    }
    vector->i[i] = (uintptr_t) func + at_module_final;
    return 0;
}

int _clib_at_destruction(vprocp func)
{
    /* For module clients, constructor-registered destructors always need to
     * be deferred until module finalisation */
    return atexit_common(func, &_last_exitvector, (_kernel_processor_mode() & 0xF) != 0 || _kernel_client_is_module());
}

int atexit(vprocp func)
{
    return atexit_common(func, &_last_exitvector, (_kernel_processor_mode() & 0xF) != 0);
}

int at_quick_exit(vprocp func)
{
    /* It's an error to call quick_exit() from SVC mode, so it doesn't make any
     * sense to register quick exit callbacks to be called after module
     * finalisation. */
    return atexit_common(func, &_last_qxitvector, 0);
}

static void iterate_over_exit_fns(struct exitvector **last_vector, int do_call)
{
    int mode = ((_kernel_processor_mode() & 0xF) != 0);
    int updating = 1;
    for (struct exitvector *vector = *last_vector, *prev; vector != NULL; vector = prev)
    {
        for (size_t i = EXIT_LIMIT; i-- > 0; )
        {
            if (vector->p[i])
            {
                if ((vector->i[i] & 3) != mode)
                {
                    /* We're doing USR mode module exit but encountered a routine to
                     * be called when exiting SVC mode - leave for later */
                    updating = 0;
                }
                else
                {
                    /* Overwrite with NULL before calling, to prevent recursion in case of
                     * aborts during exit handlers */
                    vprocp fn = (vprocp) (vector->i[i] &~ 3);
                    vector->p[i] = NULL;
                    if (do_call)
                        fn();
                }
            }
        }

        prev = vector->prev;
        if (prev != NULL)
        {
            if (updating)
                *last_vector = prev;
            if (!_kernel_client_is_module())
                free(vector);
        }
        /* Else, since the root link on the chain is statically allocated, neither
         * free it nor push last_vector back before it */
    }
}

void _exit_call_exit_fns(void)
{
    iterate_over_exit_fns(&_last_qxitvector, 1);
    iterate_over_exit_fns(&_last_exitvector, 1);
}

void exit(int n)
{   /* Stop any USR mode at_quick_exit's being called */
    iterate_over_exit_fns(&_last_qxitvector, 0);
    _exit(n);
}

void quick_exit(int n)
{   /* Stop any USR mode atexit's being called */
    iterate_over_exit_fns(&_last_exitvector, 0);
    _exit(n);
}

void _Exit(int n)
{
    iterate_over_exit_fns(&_last_qxitvector, 0);
    iterate_over_exit_fns(&_last_exitvector, 0);
    _exit(n);
}

void abort(void)
{
    iterate_over_exit_fns(&_last_qxitvector, 0);
    iterate_over_exit_fns(&_last_exitvector, 0);
    raise(SIGABRT);
    exit(1);
}

int (abs)(int x) { return abs(x); }

long int (labs)(long int x) { return labs(x); }

#if 0
/* Compiler generates poo code at the minute - in machine code for now */
long long int llabs(long long int x) {
    if (x<0) return (-x);
    else return x;
}
#endif


/* end of stdlib.c */
