/* Copyright 2023 RISC OS Open Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "swis.h"

#include "mymodHdr.h"

/* This is deliberately not declared in a header file because it's not intended for user use */
extern void _clib_at_destruction(void (*)(void));

static void first_usr_atexit(void)
{
  printf("First-registered USR mode atexit\n");
}

static void usr_atexit(void)
{
  printf("USR mode atexit\n");
}

static void usr_dtor(void)
{
  printf("Destructor registered by USR mode constructor\n");
}

static void last_usr_atexit(void)
{
  printf("Last-registered USR mode atexit\n");
}

static void first_svc_atexit(void)
{
  printf("First-registered SVC mode atexit\n");
}

static void svc_atexit(void)
{
  printf("SVC mode atexit\n");
}

static void svc_dtor(void)
{
  printf("Destructor registered by SVC mode constructor\n");
}

static void last_svc_atexit(void)
{
  printf("Last-registered SVC mode atexit\n");
}

_kernel_oserror *init(const char *cmd_tail, int podule_base, void *pw)
{
  (void) cmd_tail;
  (void) podule_base;
  (void) pw;
  atexit(first_svc_atexit);
  return NULL;
}

int main(int argc, char *argv[])
{
  (void) argc;
  (void) argv;
  atexit(first_usr_atexit);

  char c = '\n';
  for (;;)
  {
    if (c == '\n')
      printf("(a)texit callback / (d)estructor / (q)uickexit / (e)xit?\n");
    c = getchar();
    switch (c)
    {
      case 'a':
        atexit(usr_atexit);
        printf("Registered an atexit callback from USR mode\n");
        break;
      case 'd':
        _clib_at_destruction(usr_dtor);
        printf("Registered a dynamic destructor from USR mode\n");
        break;
      case 'q':
      case 'e':
        goto done;
    }
  }
  done:

  atexit(last_usr_atexit);

  if (c == 'q')
    quick_exit(0);
  else
    exit(0);
}

_kernel_oserror *command(const char *arg_string, int argc, int cmd_no, void *pw)
{
  (void) argc;
  (void) cmd_no;
  (void) pw;

  switch (cmd_no)
  {
    case CMD_main:
      _swix(OS_Module, _INR(0,2), 2, "mymod", arg_string);
      break;
    case CMD_addexit:
      atexit(svc_atexit);
      printf("Registered an atexit callback from SVC mode\n");
      break;
    case CMD_adddtor:
      _clib_at_destruction(svc_dtor);
      printf("Registered a dynamic destructor from SVC mode\n");
      break;
  }

  return NULL;
}

_kernel_oserror *final(int fatal, int podule, void *pw)
{
  (void) fatal;
  (void) podule;
  (void) pw;
  atexit(last_svc_atexit);
  return NULL;
}
